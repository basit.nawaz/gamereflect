using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Player : MonoBehaviour
{

    public Rigidbody2D rb;
    public GameObject PlayerCamera;
    public PhotonView photonView;
    public SpriteRenderer sr;
    public Text PlayerNameText;

    //public float MovementSpeed = 1;

    public bool isGrounded = false;
    public float moveSpeed = 2;
    public float JumpForce;


    private void Awake()
    {
        if(photonView.isMine)
        {
            PlayerCamera.SetActive(true);
            PlayerNameText.text = PhotonNetwork.playerName;
        } 
        
        else
       {
           PlayerNameText.text = photonView.owner.name;
           PlayerNameText.color = Color.cyan;
       }
    }

    private void Update()
    {
        
        if (photonView.isMine)
        {
            checkInput(); 
        }
          
    }

    private void checkInput()
    {
        var movement = Input.GetAxisRaw("Horizontal");
        transform.position += new Vector3(movement, 0, 0) * Time.deltaTime * moveSpeed;

        if(Input.GetKeyDown(KeyCode.A))
        {
            sr.flipX = true;
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            sr.flipX = false;
        }
    }

 

}
